import gym
import gym_donkeycar
import numpy as np
from loguru import logger

env = gym.make("donkey-generated-roads-v0")
obs = env.reset()
for t in range(100):
    action = np.array([0.0, 0.5])  # drive straight with small speed
    obs, reward, done, info = env.step(action)  # execute the action
    logger.debug((obs, reward, done))
    if done:
        obs = env.reset()
env.close()
