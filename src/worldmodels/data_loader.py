import glob
from pathlib import Path

import cv2
import numpy as np


def pre_process_image(img: np.ndarray) -> np.ndarray:
    return img.astype(np.float32) / 255.0  # Rescale to 0-1.0


class DataLoader(object):
    def __init__(self, image_dir: Path, batch_size: int = 32):
        super(DataLoader, self).__init__()
        self.image_dir = image_dir
        self.batch_size = batch_size

    def _make_observation(self, image_path) -> np.ndarray:
        img = cv2.imread(image_path)
        if img is None:
            raise ValueError(f"Could not load {image_path}.")
        return pre_process_image(img)

    def __iter__(self):
        return self

    def __next__(self):
        images = list(glob.glob(f"{self.image_dir}/*.jpg"))  # Load list of image paths
        if images is None or len(images) == 0:
            raise ValueError(f"No images were found in {self.image_dir}")
        idx = list(np.random.choice(len(images), self.batch_size))  # Randomise images
        return [self._make_observation(images[i]) for i in idx]
