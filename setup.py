
# -*- coding: utf-8 -*-

# DO NOT EDIT THIS FILE!
# This file has been autogenerated by dephell <3
# https://github.com/dephell/dephell

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

readme = ''

setup(
    long_description=readme,
    name='donkeycar',
    version='0.1.0',
    description='A reinforcement learning project that demonstrates a driverless remote control car and a variational auto-encoder.',
    python_requires='==3.*,>=3.7.0',
    author='Phil Winder',
    author_email='phil@WinderResearch.com',
    packages=['arraffin', 'arraffin.vae', 'examples', 'worldmodels'],
    package_dir={"": "src"},
    package_data={},
    install_requires=['ffmpeg-python==0.*,>=0.2.0', 'gym-donkeycar', 'joblib==0.*,>=0.15.1', 'loguru==0.*,>=0.5.0', 'opencv-python==4.*,>=4.2.0', 'optuna==1.*,>=1.5.0', 'simple-pid==0.*,>=0.2.4', 'stable-baselines==2.*,>=2.10.0', 'tensorflow==1.15.3'],
    dependency_links=['git+https://github.com/tawnkramer/gym-donkeycar.git@f552b4bfc1a9bb519a5e42c7af8c23dd6c9ef304#egg=gym-donkeycar'],
    extras_require={"dev": ["black==19.*,>=19.10.0.b0"]},
)
