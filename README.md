# Teach an RC Car to Race Using Only a Camera

A [Reinforcement Learning](https://rl-book.com/?utm_source=git&utm_medium=repository&utm_campaign=donkeycar) project by [Winder Research](https://winderresearch.com/?utm_source=git&utm_medium=repository&utm_campaign=donkeycar).

| `donkey-generated-roads-v0` | `donkey-generated-track-v0` |
|---|---|
|![](images/trained_road.gif)|![](images/trained_track.gif)|

## Installation

1. Download the Unity DonkeyCar simulation for your operating system from: [gym-donkeycar](https://github.com/tawnkramer/gym-donkeycar/releases/tag/v2020.5.16)
2. Git clone this repository
3. Create a Python 3.7 virtual environment and `python setup.py install`

Alternatively, you can run `poetry install`.

## Reinforcement Learning Policy Training

With the provided [hyperparameters](hyperparams/sac.yaml) the DonkeyCar should learn a reasonable policy within 5000 environment steps (approximately 10 minutes on my macbook). If you leave it for longer it should be able to solve the basic tracks.

1. Start the donkey_sim binary and leave it at the main page.
2. Choose a saved VAE model to match the track you want to train upon.
3. `python train_sac.py`

See the `--help` for configuration options.

This process will continue until you `CTRL+C` at which point it will dump the model to a file. This takes anywhere from five minutes to one hour on my 2014 MacBook Pro, depending on how decent you want your policy.

### FAQ

- **New Hyper-Parameters?**: Alter the [hyperparameters](hyperparams/sac.yaml) and run again.
- **New Tracks?**: Pass a different VAE model (each environment needs a new VAE) and environment ID to the train function.

## Variational Auto-Encoder

![](images/vae_video.gif)

This project uses a VAE as the state representation mechanism. To train a new VAE, you first need to generate some training data by following these steps:

1. Launch the donkey_sim application.
2. Click on the `Log dir` button and set the log dir. (This is `PATH_TO_IMAGES_DIR` below)
3. Launch a track.
4. Click on the `[METHOD] w Rec` option.
5. Watch the log count at the bottom left. You want about 10,000 observations with as much diversity as possible. Bonus marks if you get your kids to perform this step! :-D

Now you have the training data, you can train the VAE. Run the (minimal) command:

1. `python train_vae.py --training_data_dir=${PATH_TO_IMAGES_DIR}`

This will continue until you `CTRL+C` at which point the model will be saved to `vae.pkl`. Tensorboard logs will be saved in the `log` directory alongside example reconstructed images created during training. For more configuration, see the help.

This will need about 10,000 training steps to produce a half decent representation. This takes about an hour on my 2014 MacBook pro.

### Pre-Trained VAE Models

|Setting|Value|
|---|---|
|`z_size`|32|

| Environment | Link |
|---|---|
|donkey-generated-roads-v0| [Download](pretrained-models/vae/vae-donkey-generated-roads-v0-32.pkl) |
|donkey-warehouse-v0| [Download](pretrained-models/vae/vae-donkey-warehouse-v0-32.pkl) |
|donkey-avc-sparkfun-v0|[Download](pretrained-models/vae/vae-donkey-avc-sparkfun-v0-32.pkl)|
|donkey-generated-track-v0|[Download](pretrained-models/vae/vae-donkey-generated-track-v0-32.pkl)|

## Monitoring

View performance metrics by running:

```bash
tensorboard --logdir ./${LOG_DIR}
```

And browse to [http://localhost:6006](http://localhost:6006)

### Visualising the VAE Reconstruction

During the VAE training you can see reconstructed images. But I also created a function to generate a video of what the agent sees during training.

```bash
python plot_vae_video.py --environment_id=donkey-generated-track-v0 --vae_path=pretrained-models/vae/vae-donkey-generated-track-v0-32.pkl --model_path=pretrained-models/policy/policy-donkey-generated-track-v0-32.zip --monitoring_dir=monitoring
```

## Credits

This was heavily inspired by several people. I was looking for an example like that provided VAE-based state representation like in David Foster's excellent book [Generative Deep Learning](https://learning.oreilly.com/library/view/generative-deep-learning/9781492041931/). Then I came across [Antonin Raffin's repo](https://github.com/araffin/learning-to-drive-in-5-minutes) which pretty much nailed a simple VAE example. That was the starting point for all this code, plus code from Antonin's other project [stable-baselines](https://github.com/hill-a/stable-baselines). Thank you Antonin.

But this wouldn't nearly be as cool if Tawn Kramer hadn't created the [DonkeyCar Unity simulation](https://github.com/tawnkramer/gym-donkeycar), so thank you to him too.

Icons made by <a href="https://www.flaticon.com/authors/payungkead" title="Payungkead">Payungkead</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

## Future Development/Ideas

- Migrate to `pytorch`. Tensorflow drives me mad.
- New reward function for DonkeyCar, it doesn't work very well.
- "act-or-continue" to smooth out actions
- Improve VAE, better structure and I think it could result in a smaller output dimension.
- I really wanted to do this in stable-baselines3, but they weren't quite ready. Hopefully I will get chance to do this in the future.